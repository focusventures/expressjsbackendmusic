import Vue from "vue";
import Router from "vue-router";

import Overview from "@/views/Overview.vue";
import Artists from "@/views/Artists.vue";
import Songs from "@/views/Songs.vue";
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "overview",
      component: Overview
    },
    {
      path: "/artists",
      name: "artists",
      component: Artists
    },
    {
      path: "/songs",
      name: "songs",
      component: Songs
    }
  ],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  }
});
