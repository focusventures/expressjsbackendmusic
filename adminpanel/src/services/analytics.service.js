import axios from "axios";

const apiClient = axios.create({
  baseURL: "https://newwellmusic.com/api",
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json"
  }
});

export default {
  getArtistAnalytics() {
    return apiClient.get("/artist/analytics");
  },
  getSongAnalytics() {
    return apiClient.get("/song/analytics");
  },
  getRegistrationCount() {
    return apiClient.get("/artist/regcount");
  }
};
