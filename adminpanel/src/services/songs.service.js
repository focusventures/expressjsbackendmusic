import axios from "axios";

const apiClient = axios.create({
  baseURL: "https://newwellmusic.com/api",
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json",
    Authorization:
      "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjVkMzZmZWE3MTVhZmRmYjg5ZjcwZjJmZCIsImVtYWlsIjoicGF0b0BwYXRvLmNvbSJ9LCJpYXQiOjE1NjYzMDU3ODB9.3WOzUd_2h3-g5xXDKmeov3-otxVYX57A4XhMkHFajkA"
  }
});

export const getAllSongs = (perPage, page, status) => {
  return apiClient.get(
    `/song?perPage=${perPage}&page=${page}&status=${status}`
  );
};
export const verifySong = (id, type) => {
  return apiClient.put(`/admin/verify/song/${id}/${type}`);
};
