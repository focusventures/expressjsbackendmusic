const express = require("express"),
  mongoose = require("mongoose"),
  morgan = require("morgan"),
  cors = require("cors"),
  bodyParser = require("body-parser"),
  config = require("config"),
  passport = require("passport"),
  axios = require("axios");

const db = config.get("db.name");
const PORT = process.env.PORT || config.get("app.PORT");
const password = config.get("db.password");

mongoose.connect(
  `mongodb://newwellAdmin:${password}@newwellmusic-shard-00-00-zsu9n.gcp.mongodb.net:27017,newwellmusic-shard-00-01-zsu9n.gcp.mongodb.net:27017,newwellmusic-shard-00-02-zsu9n.gcp.mongodb.net:27017/${db}?ssl=true&replicaSet=NewwellMusic-shard-0&authSource=admin&retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useCreateIndex: true
  }
);

const app = express();

app.use(passport.initialize());
app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use("/files", express.static("public"));

app.use(cors());

// Routers
const artistRouters = require("./routes/artist.routes");
const adminRouters = require("./routes/admin.routes");
const userRoutes = require("./routes/user.routes");
const songRoutes = require("./routes/song.routes");
const albumRoutes = require("./routes/album.routes");

// File Upload Management

// Route handling
app.use("/api/artist", artistRouters);
app.use("/api/admin", adminRouters);
app.use("/api/user", userRoutes);
app.use("/api/song", songRoutes);
app.use("/api/album", albumRoutes);

app.get("/bitly", async (req, res) => {
  res.redirect("https://google.com");
  const { data } = await axios.get(
    "https://api-ssl.bitly.com/v4/bitlinks/bit.ly/Pksdsdsd/countries",
    {
      headers: {
        Authorization: "Bearer f4daeaf2b6c9be58ef84f6b8fe1abcfdd8edf46b"
      }
    }
  );
  const { metrics } = data;
  console.log(metrics);
});

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
