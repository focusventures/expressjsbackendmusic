const Admin = require("../models/admin.model");
const Artist = require("../models/artist.model");
const Song = require("../models/song.model");

const passport = require("../middleware/admin.auth");
const jwt = require("jsonwebtoken");

class AdminController {
  static async create(req, res) {
    try {
      console.log(req.user);
      const newAdmin = Admin(req.body);
      const savedDoc = await newAdmin.save();
      return res.status(201).json(savedDoc);
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  static async index(req, res) {
    try {
      const docs = await Admin.find({})
        .select("-password")
        .exec();
      return res.status(200).json(docs);
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  static async verifyArtist(req, res) {
    try {
      const { _id, stage } = req.params;
      const update = {};

      switch (stage) {
        case "1":
          update.status = "verified";
          break;

        case "2":
          update.status = "rejected";
          break;
        default:
          update.status = "unverified";
          break;
      }
      const { n, nModified } = await Artist.updateOne({ _id }, update).exec();

      if (n == 1 && nModified == 1) {
        return res.status(200).json({
          message: "Successfully Updated",
          ...update
        });
      } else {
        return res.status(200).json({
          message: "Not modified",
          ...update
        });
      }
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  static async verifySong(req, res) {
    try {
      const { _id, stage } = req.params;
      const update = {};

      switch (stage) {
        case "1":
          update.status = "verified_newwell";
          break;
        case "2":
          update.status = "verified_distro";
          break;
        case "3":
          update.status = "rejected";
          break;
        default:
          update.status = "unverified";
          break;
      }
      const response = await Song.updateOne({ _id }, update).exec();
      const { n, nModified } = response;
      if (n == 1 && nModified == 1) {
        return res.status(200).json({
          message: "Successfully Updated",
          ...update,
          ...response
        });
      } else {
        return res.status(200).json({
          message: "Not modified",
          ...update,
          ...response
        });
      }
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  static login(req, res) {
    passport.authenticate("admin_login", async (err, user, info) => {
      try {
        console.log("started 2");
        if (err || !user)
          return res.status(404).json({
            message: info
          });

        req.login(user, { session: false }, async error => {
          if (error)
            return res.status(404).json({
              error
            });
          //We don't want to store the sensitive information such as the
          //user password in the token so we pick only the email and id
          const body = { _id: user._id, email: user.email };
          //Sign the JWT token and populate the payload with the user email and id
          const token = jwt.sign({ user: body }, "top_secret");
          //Send back the token to the user
          return res.json({ token });
        });
      } catch (err) {
        return res.status(500).json({ err });
      }
    })(req, res);
  }
}

module.exports = AdminController;
