const artistsAnalytics = [
  //
  {
    $group: {
      _id: "$status",
      count: { $sum: 1 }
    }
  }
];

const registrationCount = [
  {
    $group: {
      _id: "$registrationDate",
      count: { $sum: 1 }
    }
  },
  {
    $sort: { _id: 1 }
  }
];

module.exports = { artistsAnalytics, registrationCount };
