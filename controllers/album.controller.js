const Album = require('../models/album.model')
const Song = require('../models/song.model')

class AlbumController {
  static async index(req, res) {
    try {
      const albums = await Album.find({}).exec()
      return res.status(200).json(albums)
    } catch (error) {
      return res.status(500).json({ error })
    }
  }

  static async create(req, res) {
    try {
      const newAlbum = new Album({
        ...req.body,
        artist: req.user.id
      })
      const savedDoc = await newAlbum.save()
      return res.status(201).json(savedDoc)

    } catch (error) {
      return res.status(500).json({ error })
    }
  }

  static async showArtistAlbums(req, res) {
    try {
      const { artistId: artist } = req.params
      const albums = await Album.find({ artist }).populate('artist', "name email -_id").exec()
      return res.status(200).json(albums)
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  static async showSongsInAlbum(req, res) {
    try {
      const { albumId } = req.params
      const songs = await Song.find({ 'album.albumId': albumId }).exec()
      return res.status(200).json(songs)
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  static async uploadAlbumArt(req, res) {
    try {
      const { albumId } = req.params;
      const { _id: artistId } = req.user

      const artistHasTheAlbum = await AlbumController.artistHasAlbum(albumId, artistId);

      if (!artistHasTheAlbum) return req.status(404).json({
        message: "Artist does not have such an album"
      })

      const updateResults = await Album.updateOne(
        {
          _id: albumId,
          artist: artistId
        }, {
          albumArt: req.file.gcsUrl
        })
      return res.status(200).json({
        success: 'Album Art Updated',
        updateResults
      })
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  static async artistHasAlbum(albumId, artistId) {
    const album = await Album.find({
      _id: albumId,
      artist: artistId
    }).exec()

    if (!album) return false;
    else return true;
  }
}

module.exports = AlbumController