const Artist = require("../models/artist.model");

const passport = require("../middleware/artist.auth");
const jwt = require("jsonwebtoken");
const Joi = require("joi");
const bcrypt = require('bcryptjs')

const { registerUser } = require("../middleware/mailchimp");
const { transporter, gmailCredentials } = require("../middleware/nodemailer");
// require("../middleware/artist.auth");

class ArtistController {
  static async index(req, res) {
    try {
      let { perPage, page, status } = req.query;

      const search = {};
      if (perPage) perPage = parseInt(perPage);
      if (page) page = parseInt(page);
      if (status) search.status = status;

      const docs = await Artist.find(search)
        .limit(perPage || 10)
        .skip(perPage * ((page || 1) - 1))
        .select("-password")
        .sort("-_id")
        .exec();
      const total_count = await Artist.find(search)
        .countDocuments()
        .exec();
      return res.json({
        results: docs,
        meta: {
          current_page: page || 1,
          per_page: perPage || 10,
          total_count
        }
      });
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  static async analytics(req, res) {
    const { artistsAnalytics } = require("./aggregations/artists_analytics");
    try {
      const results = await Artist.aggregate(artistsAnalytics).exec();
      const final = {
        unverified: 0,
        verified: 0,
        rejected: 0
      };
      let total = 0;

      results.forEach(item => {
        final[item._id] = item.count;
        total += item.count;
      });

      final.total = total;

      return res.json(final);
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  static async regCount(req, res) {
    const { registrationCount } = require("./aggregations/artists_analytics");

    try {
      const results = await Artist.aggregate(registrationCount).exec();
      return res.json(results);
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  static async create(req, res) {
    try {
      // const { name, genre, bio, profilePic } = req.body;
      // const newArtist = Artist(req.body);
      const validationSchema = Joi.object()
        .options({ abortEarly: false })
        .keys({
          name: Joi.string()
            .min(3)
            .max(15)
            .required(),
          password: Joi.string().min(6),
          phoneNumber: Joi.string()
            .min(10)
            .max(15),
          email: Joi.string().email({ minDomainAtoms: 2 })
        });

      const { error, result } = Joi.validate(req.body, validationSchema);

      if (error) {
        return res.status(400).json(error);
      }
      const artist = await Artist.findOne({ email: req.body.email }).exec();
      if (artist) {
        return res.status(400).json({
          details: "Email is already used"
        });
      }

      const today = new Date();
      today.setHours(0, 0, 0, 0);

      const newArtist = new Artist({
        ...req.body,

        registrationDate: today
      });
      const savedDoc = await newArtist.save();

      // const mailOptions = {
      //   from: 'newwellstudio@gmail.com', // sender address
      //   to: req.body.email, // list of receivers
      //   subject: 'Welcome To Newwell', // Subject line
      //   html: `
      //     <head>
      //       <meta name="viewport" content="width=device-width, initial-scale=1">
      //       <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
      //       <style>
      //         * {
      //           font-family: 'Montserrat', sans-serif;
      //         }
      //       </style>
      //     </head>
      //     <body>
      //       <h1>Welcome To Newwell</h1>
      //       <p>Thank you ${req.body.name} for choosing Newwell.</p>
      //     </body>
      //   `// plain text body
      // }

      // transporter.sendMail(mailOptions, (err, info) => {
      //   if (err) console.log(err);
      //   else console.log(info)
      //   console.log(gmailCredentials)
      // })
      registerUser(req.body.email, req.body.name);
      return res.status(201).json(savedDoc);
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  static async uploadPic(req, res) {
    try {
      //*** Saving Of Image */
      // const { profilePic } = req.files;

      // const newImageName = `${profilePic.md5}${
      //   profilePic.mimetype === "image/jpeg" ? ".jpg" : ".png"
      //   }`;

      // const pathToImage = `${__dirname}/../public/images/${newImageName}`;
      // profilePic.mv(pathToImage, err => {
      //   if (err) return console.log("There was an error", err);
      //   console.log("Succesfully saved file");
      // });
      // //*** End of Saving Of Image */

      const { _id } = req.user;
      console.log(_id);
      const updateResults = await Artist.updateOne(
        { _id },
        {
          profilePic: req.file.gcsUrl
        }
      ).exec();
      return res.status(200).json(updateResults);
    } catch (error) {
      // error = JSON.stringify(error)
      return res.status(500).json({ error });
    }
  }

  static async show(req, res) {
    try {
      const { _id } = req.params;
      console.log(_id);
      const doc = await Artist.findById(_id).exec();
      return res.json(doc);
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  static async update(req, res) {
    try {
      const { _id } = req.user;
      console.log(req.user);
      const updatedDoc = await Artist.updateOne(
        { _id },
        { ...req.body, verified: false }
      ).exec();
      return res.status(202).json(updatedDoc);
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  static signIn(req, res) {
    console.log("started");

    passport.authenticate("artist_login", async (err, user, info) => {
      try {
        console.log("started 2");
        if (err || !user) return res.status(404).json(info);

        req.login(user, { session: false }, async error => {
          if (error)
            return res.status(404).json({
              error
            });
          //We don't want to store the sensitive information such as the
          //user password in the token so we pick only the email and id
          const body = { _id: user._id, email: user.email };
          const { _id, name } = user;
          //Sign the JWT token and populate the payload with the user email and id
          const token = jwt.sign({ user: body }, "top_secret");
          //Send back the token to the user
          return res.json({
            user: {
              _id,
              name
            },
            token
          });
        });
      } catch (err) {
        return res.status(500).json({ err });
      }
    })(req, res);
  }

  static async forgotPassword(req, res) {
    try {
      const { email } = req.body;
      const crypto = require("crypto");
      let passwordToken = await crypto.randomBytes(10);
      passwordToken = passwordToken.toString("hex");
      const { _id } = await Artist.findOne({ email }).exec();
      const { nModified } = await Artist.updateOne(
        { _id },
        {
          passwordToken
        }
      );

      if (nModified === 1) {
        const link = `https://newwellmusic.com/password_reset?id=${_id}&token=${passwordToken}`;
        const mailOptions = {
          from: "newwellstudio@gmail.com", // sender address
          to: email, // list of receivers
          subject: "Password Recovery", // Subject line
          html: `
          <head>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
            <style>
              * {
                font-family: 'Montserrat', sans-serif;
              }
            </style>
          </head>
          <body>
            <h1>Password Recovery</h1>
            <p>Use this link to update your password: <a href=${link}>${link}</a></p>
          </body>
        ` // plain text body
        };

        transporter.sendMail(mailOptions, (err, info) => {
          if (err) console.log(err);
          else return res.json({ message: "Successfully Sent email" });
          console.log(gmailCredentials);
        });
      } else return res.status(500).json({ error: "Unsuccessful" });
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  static async resetPassword(req, res) {
    try {
      const {token , _id, newPassword} = req.body;
      const {passwordToken} = await Artist.findById(_id).exec();
      console.log(passwordToken)
      if (passwordToken === token) {
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(newPassword, salt);
        const {nModified} = await Artist.updateOne({_id}, {
          password: hash,
          passwordToken: ""
        });
        if (nModified === 1) {
          return res.json({
            message: 'updated successfully'
          })
        } else {
          return res.status(500).json({
            error: 'could not update'
          })
        }
      } else {
        return res.status(401).json({
            error: 'wrong token'
          })
      }
    } catch (error) {
      return res.status(500).json({ error });
    }
  }
}
module.exports = ArtistController;
