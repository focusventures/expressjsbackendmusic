const Song = require("../models/song.model");
const Album = require("../models/album.model");

class SongController {
  static async index(req, res) {
    try {
      // let { perPage, page } = req.query;
      // const totalArtists = await Artist.find({})
      //   .estimatedDocumentCount()
      //   .exec();

      let { perPage, page, status } = req.query;

      // const docs = await Song.find({})
      //   .populate("artist", "name email -_id")
      //   .exec();
      const search = {};
      if (perPage) perPage = parseInt(perPage);
      if (page) page = parseInt(page);
      if (status) search.status = status;

      const docs = await Song.find(search)
        .limit(perPage || 10)
        .skip(perPage * ((page || 1) - 1))
        .populate("artist", "name email -_id")
        .sort("-_id")
        .exec();
      const total_count = await Song.find(search)
        .countDocuments()
        .exec();
      return res.json({
        results: docs,
        meta: {
          current_page: page || 1,
          per_page: perPage || 10,
          total_count
        }
      });
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  static async analytics(req, res) {
    const { artistsAnalytics } = require("./aggregations/artists_analytics");
    try {
      const results = await Song.aggregate(artistsAnalytics).exec();

      const final = {
        unverified: 0,
        verified_newwell: 0,
        rejected: 0,
        verified_distro: 0
      };
      let total = 0;

      results.forEach(item => {
        final[item._id] = item.count;
        total += item.count;
      });

      final.total = total;

      return res.json(final);
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  static async create(req, res) {
    // const { albumArt, audioFile } = req.files;
    // const audioFileName = `${audioFile.md5}.mp3`;
    // const albumArtName = `${albumArt.md5}${
    //   albumArt.mimetype === "image/jpeg" ? ".jpg" : ".png"
    // }`;

    // const audioFilePath = `${__dirname}/../public/audio/${audioFileName}`;
    // const albumArtPath = `${__dirname}/../public/images/${albumArtName}`;

    // audioFile.mv(audioFilePath, err => {
    //   if (err) return res.status(500).json({ err });
    //   console.log("Audio File saved");
    //   albumArt.mv(albumArtPath, async err => {
    //     if (err) return res.status(500).json({ err });
    //     console.log("Image File saved");
    //     const newSong = new Song({
    //       ...req.body,
    //       audioFile: `http://${req.headers.host}/files/audio/${audioFileName}`,
    //       albumArt: `http://${req.headers.host}/files/images/${albumArtName}`,
    //       artist: req.user.id,
    //       verifiedNewell: false,
    //       verifiedDistro: false
    //     });
    //     const savedDoc = await newSong.save();
    //     return res.status(201).json(savedDoc);
    //   });
    // });
    try {
      if (req.body.album) {
        const albumId = req.body.album;
        const { albumArt } = await Album.findById(albumId).exec();
        req.body = {
          ...req.body,
          album: {
            albumType: "album",
            albumId
          },
          albumArt
        };
      }

      const newSong = new Song({
        ...req.body,
        artist: req.user.id,
        verifiedNewell: false,
        verifiedDistro: false
      });
      const savedDoc = await newSong.save();
      return res.status(201).json(savedDoc);
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  static async showArtistSongs(req, res) {
    try {
      const { artistId: artist } = req.params;
      const songs = await Song.find({ artist })
        .populate("artist", "name email -_id")
        .exec();
      return res.status(200).json(songs);
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  static async uploadAlbumArt(req, res) {
    try {
      const { songId } = req.params;
      const { _id: artistId } = req.user;

      const artistHasTheSong = await SongController.artistHasSong(
        songId,
        artistId
      );

      if (!artistHasTheSong)
        return req.status(404).json({
          message: "Artist does not have such a song"
        });

      const updateResults = await Song.updateOne(
        {
          _id: songId,
          artist: artistId
        },
        {
          albumArt: req.file.gcsUrl
        }
      );
      return res.status(200).json({
        success: "Album Art Updated",
        updateResults
      });
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  static async updateAudioFile(req, res) {
    try {
      const { songId } = req.params;
      const { _id: artistId } = req.user;

      const artistHasTheSong = await SongController.artistHasSong(
        songId,
        artistId
      );

      if (!artistHasTheSong)
        return req.status(404).json({
          message: "Artist does not have such a song"
        });

      const updateResults = await Song.updateOne(
        {
          _id: songId,
          artist: artistId
        },
        {
          audioFile: req.file.gcsUrl
        }
      );
      return res.status(200).json({
        success: "Audio File Updated",
        updateResults
      });
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  static async artistHasSong(songId, artistId) {
    const song = await Song.find({
      _id: songId,
      artist: artistId
    }).exec();

    if (!song) return false;
    else return true;
  }
}

module.exports = SongController;
