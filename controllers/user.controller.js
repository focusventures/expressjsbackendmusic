const config = require("config");
const API_KEY = config.get("authy.API_KEY");
const authy = require("authy")(API_KEY);

const User = require("../models/user.model");

class UserController {
  static async create(req, res) {
    let { name, phoneNumber, email, countryCode } = req.body;

    countryCode = countryCode ? countryCode : "254";
    email = email ? email : "yes@yes.com";

    authy.register_user(email, phoneNumber, countryCode, function(err, resp1) {
      // res = {user: {id: 1337}} where 1337 = ID given to use, store this someplace
      if (err) {
        return res.status(400).json({ err });
      }
      const userId = resp1.user.id;
      authy.request_sms(userId, async function(err, resp2) {
        if (err) {
          return res.status(400).json({ err });
        }
        const newUser = User({
          name,
          phoneNumber,
          countryCode,
          email,
          userId
        });

        const savedDoc = await newUser.save();

        return res.status(201).json({ resp: resp2, savedDoc, userId });
      });
    });
  }

  static async verify(req, res) {
    const { phoneNumber, token } = req.params;

    const user = await User.findOne({
      phoneNumber
    }).exec();

    authy.verify(user.userId, token, function(err, resp) {
      if (err) {
        return res.status(400).json({ err });
      }
      return res.status(200).json({ resp, err });
    });
  }
}

module.exports = UserController;
