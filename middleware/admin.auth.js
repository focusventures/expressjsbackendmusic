const passport = require("passport");
const { Strategy: LocalStrategy } = require("passport-local");

const { Strategy: JWTStrategy, ExtractJwt } = require("passport-jwt");

const Admin = require("../models/admin.model");

passport.use(
  "admin_login",
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password"
    },
    async (email, password, done) => {
      try {
        console.log("started");
        const admin = await Admin.findOne({ email });

        if (!admin) return done(null, false, { message: "User Not Found" });

        const validate = await admin.isValidPassword(password);

        if (!validate) return done(null, false, { message: "Wrong Password" });

        return done(null, admin, { message: "Logged in Successfully" });
      } catch (error) {
        done(error);
      }
    }
  )
);

passport.use(
  "jwt_admin",
  new JWTStrategy(
    {
      secretOrKey: "top_secret",
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
    },
    async (decoded, done) => {
      const { _id } = decoded.user;
      const admin = await Admin.findById(_id).exec();

      if (!admin) done("No Such Admin");
      else done(null, admin);
    }
  )
);

module.exports = passport;
