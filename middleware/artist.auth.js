const passport = require("passport");
const { Strategy: LocalStrategy } = require("passport-local");

const { Strategy: JWTStrategy, ExtractJwt } = require("passport-jwt");

const Artist = require("../models/artist.model");

passport.use(
  "artist_login",
  new LocalStrategy(
    {
      usernameField: "email",
      passwordField: "password"
    },
    async (email, password, done) => {
      try {
        console.log("started");
        const artist = await Artist.findOne({ email });

        if (!artist) return done(null, false, { message: "User Not Found" });

        const validate = await artist.isValidPassword(password);

        if (!validate) return done(null, false, { message: "Wrong Password" });

        return done(null, artist, { message: "Logged in Successfully" });
      } catch (error) {
        done(error);
      }
    }
  )
);

passport.use(
  "jwt_artist",
  new JWTStrategy(
    {
      secretOrKey: "top_secret",
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
    },
    async (decoded, done) => {
      console.log(decoded.user)
      const { _id } = decoded.user;
      console.log(_id)
      const artist = await Artist.findById(_id).exec();
      console.log(artist)
      if (!artist) done("No Such Artist");
      else done(null, artist);
    }
  )
);

module.exports = passport;
