class AuthyHelper {
  static async registerUser(
    phoneNumber,
    countryCode = "254",
    email = "yes@yes.com"
  ) {
    let response;
    await authy.register_user(email, phoneNumber, countryCode, function (
      err,
      res
    ) {
      // res = {user: {id: 1337}} where 1337 = ID given to use, store this someplace
      if (err) {
        return;
      }
      authy.request_sms(res.user.id, function (err, res) {
        if (err) {
          return;
        }
        response = res;
      });
    });
    return response;
  }
}

module.exports = AuthyHelper;
