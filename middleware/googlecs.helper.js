const url = require('url')

const { Storage } = require('@google-cloud/storage');

const config = require('config');

const BUCKET_NAME = config.get('app.googlecs.bucket')
const GOOGLE_CLOUD_PROJECT_ID = config.get('app.googlecs.projectId'); // Replace with your project ID
const GOOGLE_CLOUD_KEYFILE = './config/GCS.json'

// const storage = GoogleCloudStorage({
//   projectId: GOOGLE_CLOUD_PROJECT_ID,
//   keyFilename: GOOGLE_CLOUD_KEYFILE,
// });

const storage = new Storage({
  projectId: GOOGLE_CLOUD_PROJECT_ID,
  keyFilename: GOOGLE_CLOUD_KEYFILE,
})

exports.getPublicUrl = (fileName) => `https://storage.googleapis.com/${BUCKET_NAME}/${fileName}`;

exports.sendUploadToGCS = (req, res, next) => {
  if (!req.file) {
    return next();
  }

  let folderToUploadTo;
  switch (req.uploadType) {
    case 'artistProfile':
      folderToUploadTo = 'images/artistProfileImages';
      break;
    case 'albumArt':
      folderToUploadTo = 'images/albumArtImages';
      break;
    case 'audio':
      folderToUploadTo = 'audio';
      break;
    default:
      folderToUploadTo = '';
      break;
  }

  const bucket = storage.bucket(BUCKET_NAME);
  const gcsFileName = `${folderToUploadTo}/${Date.now()}-${Math.random().toString(36).substring(7)}`;
  const file = bucket.file(gcsFileName);

  const stream = file.createWriteStream({
    metadata: {
      contentType: req.file.mimetype,
    },
    resumable: true
  });

  stream.on('error', (err) => {
    req.file.cloudStorageError = err;
    console.log(err)
    next(err);
  });

  stream.on('finish', () => {
    req.file.cloudStorageObject = gcsFileName;


    req.file.gcsUrl = exports.getPublicUrl(gcsFileName);
    next();

  });

  stream.end(req.file.buffer);
};
