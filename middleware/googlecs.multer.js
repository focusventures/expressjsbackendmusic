const multer = require('multer');

const googleCS = require('multer-google-storage');

const options = {
  keyFilename: "./config/GCS.json",
  projectId: 'newwell-music',
  bucket: 'newwell-storage',
}

const uploadHandler = multer({
  storage: googleCS.storageEngine(options)
});

module.exports = uploadHandler;