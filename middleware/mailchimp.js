const Mailchimp = require('mailchimp-api-v3');
const config = require('config')

const { key, listId, dcs } = config.get('app.mailchimp')

const mailchimp = new Mailchimp(key);

const registerUser = (emailAddress, username) => {
  mailchimp.post(`/lists/${listId}/members`, {
    email_address: emailAddress,
    status: 'subscribed',
    merge_fields: {
      FNAME: username
    }
  }).then(res => console.log(res))
    .catch(err => console.log(err))
}

module.exports = {
  registerUser
}