const nodemailer = require('nodemailer')
const config = require('config')


const gmailCredentials = config.get('app.gmail')

const {
  user,
  password: pass
} = gmailCredentials;
// const user = config.get('app.gmail.user');
// const pass = config.get('app.gmail.password')
// const clientSecret = config.get('app.gmail.password')

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user,
    pass
  }
})


module.exports = { transporter }