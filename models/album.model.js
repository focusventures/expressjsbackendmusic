const mongoose = require("mongoose");

const AlbumSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  genre: String,
  albumArt: String,
  artist: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Artist"
  },
  link: String,
  status: {
    type: String,
    default: 'unverified'
  }
});

module.exports = mongoose.model("Album", AlbumSchema);
