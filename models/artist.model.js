const mongoose = require("mongoose");

const bcrypt = require("bcryptjs");

const ArtistSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  genre: String,
  bio: {
    type: String
  },
  profilePic: {
    type: String
  },
  password: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  phoneNumber: {
    type: String,
    required: true
  },
  status: {
    type: String,
    default: "unverified"
  },
  registrationDate: {
    type: Date,
    required: true
  },
  passwordToken: String
});

ArtistSchema.pre("save", async function() {
  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(this.password, salt);
  this.password = hash;
});

ArtistSchema.methods.isValidPassword = async function(password) {
  const user = this;
  //Hashes the password sent by the user for login and checks UserSchemaif the hashed password stored in the
  //database matches the one sent. Returns true if it does else false.
  const compare = await bcrypt.compare(password, user.password);
  return compare;
};

module.exports = mongoose.model("Artist", ArtistSchema);
