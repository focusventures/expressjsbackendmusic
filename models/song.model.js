const mongoose = require("mongoose");

const SongSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  genre: String,
  albumArt: String,
  audioFile: String,
  artist: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Artist"
  },
  link: String,
  status: {
    type: String,
    default: "unverified"
  },
  album: {
    albumType: {
      type: String,
      default: "single"
    },
    albumId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Album"
    }
  },
  platforms: [
    {
      name: String,
      link: String,
      _id: false
    }
  ],
  artistNames: [String],
  lyricsLanguage: String,
  lyrics: String,
  producer: {
    firstName: String,
    lastName: String,
    phoneNumber: String,
    _id: false
  },
  previouslyReleased: Boolean,
  onRecordLabel: Boolean
});

module.exports = mongoose.model("Song", SongSchema);
