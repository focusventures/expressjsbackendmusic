const mongoose = require("mongoose");

const UserModel = new mongoose.Schema({
  name: { type: String, required: true },
  phoneNumber: { type: String, required: true },
  countryCode: {
    type: String,
    default: "254"
  },
  email: {
    type: String
  },
  userId: Number
});

module.exports = mongoose.model("User", UserModel);
