const router = require("express").Router();
const passport = require("../middleware/admin.auth");

const AdminController = require("../controllers/admin.controller");

router.post(
  "/create",
  passport.authenticate("jwt_admin", { session: false }),
  AdminController.create
);
router.get(
  "/",
  passport.authenticate("jwt_admin", { session: false }),
  AdminController.index
);
router.post("/login", AdminController.login);

router.put(
  "/verify/artist/:_id/:stage",
  passport.authenticate("jwt_admin", { session: false }),
  AdminController.verifyArtist
);
router.put(
  "/verify/song/:_id/:stage",
  passport.authenticate("jwt_admin", { session: false }),
  AdminController.verifySong
);

module.exports = router;
