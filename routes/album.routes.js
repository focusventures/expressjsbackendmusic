const router = require("express").Router();

const AlbumController = require("../controllers/album.controller");

const passport = require("../middleware/artist.auth");

const gcsHelpers = require('../middleware/googlecs.helper')

const Multer = require('multer');

const multer = Multer({
  storage: Multer.MemoryStorage,
  limits: {
    fileSize: 75 * 1024 * 1024, // Maximum file size is 10MB
  },
});

router.get("/", AlbumController.index);

router.get('/:albumId', AlbumController.showSongsInAlbum)
router.post(
  "/new",
  passport.authenticate("jwt_artist", { session: false }),
  AlbumController.create
);


router.put(
  '/albumArt/:albumId',
  passport.authenticate("jwt_artist", { session: false }),
  multer.single('albumArt'),
  (req, res, next) => {
    req.uploadType = 'albumArt';
    next()
  },
  gcsHelpers.sendUploadToGCS,
  AlbumController.uploadAlbumArt
)


router.get('/artist/:artistId', AlbumController.showArtistAlbums)

module.exports = router;
