const router = require("express").Router();
const ArtistController = require("../controllers/artist.controller");

const passport = require("../middleware/artist.auth");

const gcsHelpers = require("../middleware/googlecs.helper");

const Multer = require("multer");

const multer = Multer({
  storage: Multer.MemoryStorage,
  limits: {
    fileSize: 10 * 1024 * 1024 // Maximum file size is 10MB
  }
});

// Get all Artists
router.get("/", ArtistController.index);
// Create an Artist
router.post("/new", ArtistController.create);
// Update an Artist
router.put(
  "/update",
  passport.authenticate("jwt_artist", { session: false }),
  ArtistController.update
);

router.put(
  "/uploadPic",
  passport.authenticate("jwt_artist", { session: false }),
  multer.single("profilePic"),
  (req, res, next) => {
    req.uploadType = "artistProfile";
    next();
  },
  gcsHelpers.sendUploadToGCS,
  ArtistController.uploadPic
);

router.get("/analytics", ArtistController.analytics);
router.get("/regcount", ArtistController.regCount);
// Show Artist
router.get("/:_id", ArtistController.show);

router.post("/login", ArtistController.signIn);
router.post("/forgotPassword", ArtistController.forgotPassword);
router.post("/resetPassword", ArtistController.resetPassword);
module.exports = router;
