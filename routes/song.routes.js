const router = require("express").Router();

const SongController = require("../controllers/song.controller");

const passport = require("../middleware/artist.auth");

const gcsHelpers = require("../middleware/googlecs.helper");

const Multer = require("multer");

const multer = Multer({
  storage: Multer.MemoryStorage,
  limits: {
    fileSize: 100 * 1024 * 1024 // Maximum file size is 10MB
  }
});

router.get("/analytics", SongController.analytics);

router.get("/", SongController.index);
router.post(
  "/new",
  passport.authenticate("jwt_artist", { session: false }),
  SongController.create
);

router.put(
  "/albumArt/:songId",
  passport.authenticate("jwt_artist", { session: false }),
  multer.single("albumArt"),
  (req, res, next) => {
    req.uploadType = "albumArt";
    next();
  },
  gcsHelpers.sendUploadToGCS,
  SongController.uploadAlbumArt
);

router.put(
  "/audioFile/:songId",
  passport.authenticate("jwt_artist", { session: false }),
  multer.single("audioFile"),
  (req, res, next) => {
    req.uploadType = "audio";
    next();
  },
  gcsHelpers.sendUploadToGCS,
  SongController.updateAudioFile
);

router.get("/artist/:artistId", SongController.showArtistSongs);

module.exports = router;
