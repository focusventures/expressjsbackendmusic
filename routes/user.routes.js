const router = require("express").Router();

const UserController = require("../controllers/user.controller");

router.post("/new", UserController.create);
router.get("/verify/:phoneNumber/:token", UserController.verify);

module.exports = router;
